<?php

namespace Drupal\prevent_absolute_internal_links\Plugin\Validation\Constraint;

use Drupal\Core\Url;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the LinkExternalNotInternal constraint.
 */
class LinkExternalNotInternalConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    foreach ($items as $value) {
      if (isset($value)) {
        try {
          /** @var \Drupal\Core\Url $url */
          $url = $value->getUrl();
        }
        // If the URL is malformed this constraint cannot check further.
        catch (\InvalidArgumentException $e) {
          return;
        }

        // Disallow external URLs that match the base URL.
        if ($url->isExternal()) {
          $uri = $url->getUri();
          $base_url = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
          if (strpos($uri, $base_url) === 0) {
            $this->context->addViolation($constraint->message, ['@uri' => $uri]);
          }
        }
      }
    }
  }

}
