<?php

namespace Drupal\prevent_absolute_internal_links\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * The submitted uri should be internal if it matches the site base.
 *
 * @Constraint(
 *   id = "LinkExternalNotInternal",
 *   label = @Translation("External links are not internal", context = "Validation"),
 *   type = "string"
 * )
 */
class LinkExternalNotInternalConstraint extends Constraint {

  // The message that will be shown if the URI is not internal but should be.
  public $message = '@uri is on this site and should be specified as an internal link (using autocomplete in most cases, or a relative URL)';

}
